import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormComponent } from './components/form/form.component';
import { FormPageComponent } from './pages/form-page/form-page.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { DetailsPageComponent } from './pages/details-page/details-page.component';
import { ArticleComponent } from './components/article/article.component';
import { LucideAngularModule, Plus, Trash2} from 'lucide-angular';

@NgModule({
  declarations: [
    AppComponent,
    FormComponent,
    FormPageComponent,
    HomePageComponent,
    DetailsPageComponent,
    ArticleComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    LucideAngularModule.pick({Plus, Trash2})
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
