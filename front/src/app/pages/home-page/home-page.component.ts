import { Component, OnInit } from '@angular/core';
import { ArticleService } from 'src/app/services/article.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {

  constructor(public articleService: ArticleService) {
   }


   deleteArticle(id: string){
    this.articleService.deleteArticle(id);
   }


  ngOnInit(): void {
    this.articleService.getListArticles();
  }

}


