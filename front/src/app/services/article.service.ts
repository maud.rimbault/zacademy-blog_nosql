import { Injectable } from '@angular/core';
import axios from 'axios';
import { Article } from '../models/article.model';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {
  public articles: Article[] = [];

  public async getListArticles() {
    axios.get("http://localhost:8080/api/articles/")
      .then((res) => {
        console.log(res.data);
        this.articles = res.data;
      })
      .catch((err) => {
        console.log(err);
      });
  }


  public async deleteArticle(id: string) {
    //console.log(id)
    axios.delete("http://localhost:8080/api/articles/" + id)
    //.then(() => {
    //  window.location.reload();
    //})
  }

  constructor() { }
}
