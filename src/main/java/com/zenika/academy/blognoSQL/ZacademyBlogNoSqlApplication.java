package com.zenika.academy.blognoSQL;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZacademyBlogNoSqlApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZacademyBlogNoSqlApplication.class, args);
	}

}
