package com.zenika.academy.blognoSQL.application;

import com.zenika.academy.blognoSQL.domain.model.article.Article;
import com.zenika.academy.blognoSQL.domain.model.repository.ArticleRepository;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.UUID;

@Component
public class ArticleService {
    private final ArticleRepository articleRepository;

    public ArticleService(ArticleRepository articleRepository) {
        this.articleRepository = articleRepository;
    }

    public Article createArticle(String title, String summary, String content) {
        Article newArticle = new Article(UUID.randomUUID().toString(), title, summary, content);
        articleRepository.save(newArticle);
        return newArticle;
    }

    public Iterable<Article> getAllArticles(){
        return articleRepository.findAll();
    }

    public Optional<Article> getArticle(String articleTid) {
        return articleRepository.findById(articleTid);
    }

    public void deleteArticle(String articleTid) {
        this.articleRepository.deleteById(articleTid);
    }

    public void changeArticle(String articleTid, String title, String summary, String content) {
        Article article = this.articleRepository.findById(articleTid).get();
        article.setTitle(title);
        article.setSummary(summary);
        article.setContent(content);

        this.articleRepository.save(article);
    }
}
