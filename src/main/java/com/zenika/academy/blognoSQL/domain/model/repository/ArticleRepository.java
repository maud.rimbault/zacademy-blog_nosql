package com.zenika.academy.blognoSQL.domain.model.repository;

import com.zenika.academy.blognoSQL.domain.model.article.Article;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface ArticleRepository extends MongoRepository<Article, String> {

    //public Optional<Article> findById(String id);
    //void deletebyID(String userTid);

}
