package com.zenika.academy.blognoSQL;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
public class security {

    @Bean
    SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        return http
                .antMatcher("/**")
                .csrf().disable() // CSRF protection is enabled by default in the Java configuration.
                //.authorizeRequests(authorize -> authorize
                //.authorizeRequests().antMatchers(HttpMethod.GET, "/api/articles").permitAll()
                //.anyRequest().authenticated()
                //.and()
                .authorizeRequests(aut -> aut.antMatchers(HttpMethod.GET, "/articles").permitAll()
                        .anyRequest().authenticated())
                // Activate httpBasic Authentication https://en.wikipedia.org/wiki/Basic_access_authentication
                .httpBasic()
                .and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .build();
    }
}
