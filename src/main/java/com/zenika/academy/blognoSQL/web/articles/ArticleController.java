package com.zenika.academy.blognoSQL.web.articles;

import com.zenika.academy.blognoSQL.application.ArticleService;
import com.zenika.academy.blognoSQL.domain.model.article.Article;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
//@RequestMapping("/api/articles")
@CrossOrigin(origins = "http://localhost:4200")
//@CrossOrigin
@RequestMapping("/articles")
public class ArticleController {

    private final ArticleService articleService;

    @Autowired
    public ArticleController(ArticleService articleService) {
        this.articleService = articleService;
    }

    //Creation Article
    @PostMapping
    ResponseEntity<Article> createArticle(@RequestBody ArticleDto body) {
        Article createdArticle = articleService.createArticle(body.title(), body.summary(), body.content());
        return ResponseEntity.status(HttpStatus.CREATED).body(createdArticle);
    }

    //Afficher tous les articles
    @GetMapping
    Iterable<Article> listArticles() {
        return this.articleService.getAllArticles();
    }

    //Afficher un article
    @GetMapping("/{articleTid}")
    public Optional<Article>
    findArticleById(@PathVariable("articleTid") String articleTid){
        return this.articleService.getArticle(articleTid);
    }

    //Delete un article
    @DeleteMapping("/{articleTid}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    void deleteArticle(@PathVariable String articleTid) {
        this.articleService.deleteArticle(articleTid);
    }

    @PutMapping("/{articleTid}")
    ResponseEntity<Void> updateArticle(@PathVariable String articleTid, @RequestBody ArticleDto body) {
            articleService.changeArticle(articleTid, body.title(), body.summary(), body.content());
            return ResponseEntity.ok().build();
    }

}
