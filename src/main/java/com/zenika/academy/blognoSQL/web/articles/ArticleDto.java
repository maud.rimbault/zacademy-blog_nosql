package com.zenika.academy.blognoSQL.web.articles;

public record ArticleDto(String title, String summary, String content) {
}